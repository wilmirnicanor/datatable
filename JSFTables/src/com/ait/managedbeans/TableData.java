package com.ait.managedbeans;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.ait.model.Name;

@ManagedBean
@SessionScoped
public class TableData {

	private ArrayList<Name> names;
	private String firstName;
	private String lastName;
	
	@PostConstruct
	public void init() {
		names = new ArrayList<Name>();
		
		Name name1 = new Name("William", "Dupont");
		names.add(name1);
		
		Name name2 = new Name("Harry", "Styles");
		names.add(name2);
		
		Name name3 = new Name("Jon", "Bon Jovi");
		names.add(name3);
	}

	public ArrayList<Name> getNames() {
		return names;
	}

	public void setNames(ArrayList<Name> names) {
		this.names = names;
	}
	
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String editName(Name name) {
		name.setCanEdit(true);
		return null;
	}
	
	public String saveAction() {
		for(Name name:names) {
			name.setCanEdit(false);
		}
		
		return null;
	}
	
	public String deleteName(Name name) {
		names.remove(name);
		return null;
	}
	
	
	public String addName() {
		Name name = new Name(firstName, lastName);
		
		names.add(name);
		
		return null;
	}
	
}
